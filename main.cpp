#include "thread_pool.hpp"

int main() {
	thread_pool p;
	int tmp = 0;
	for (int i = 0; i < 100; i++) {
		p.post([&] {
			for (int j = 0; j < 1000000; j++)
				tmp += 1;
			});
	}
	p.wait();

	return 0;
}