#pragma once

#include <thread>
#include <condition_variable>
#include <mutex>
#include <functional>
#include <atomic>
#include <queue>

class thread_pool {
private:
	using proc = std::function<void()>;
	std::thread thread;
	std::condition_variable task_empty_cv;
	std::mutex mtx;
	std::queue<proc> tasks;
	std::condition_variable enqueue_cv;
	std::atomic_bool stop = false;
public:
	thread_pool() {
		thread = std::thread([this] {
			while (true) {
				proc func;
				{
					std::unique_lock<std::mutex> lock(mtx);
					if (!stop && tasks.empty()) {
						task_empty_cv.notify_all();
						enqueue_cv.wait(lock);
					}
					if (stop) return;
					func = std::move(tasks.front());
					tasks.pop();
				}
				func();
			}
			});
	}
	~thread_pool() {
		stop = true;
		enqueue_cv.notify_all();
		thread.join();
	}

	template<typename F, typename... Args>
	auto post(F&& f, Args&&... args) {
		using ret_type = std::invoke_result_t<F, Args... >;
		stop = false;
		if constexpr (std::is_same_v<void, ret_type>) {
			{
				std::lock_guard lock(mtx);
				tasks.push([f = std::forward<F>(f), ...args = std::forward<Args>(args)]{ f(args...); });
			}
			enqueue_cv.notify_all();
			return;
		}
		else {
			auto prom = std::make_shared<std::promise<ret_type>>();
			auto ret = prom->get_future();
			{
				std::lock_guard lock(mtx);
				tasks.push([prom, f = std::forward<F>(f), ...args = std::forward<Args>(args)] { prom->set_value(f(args...)); });
			}
			enqueue_cv.notify_all();

			return ret;
		}
	}

	void wait() {
		if (stop) return;
		std::unique_lock<std::mutex> lock(mtx);
		task_empty_cv.wait(lock, [this] { return tasks.empty(); });
	}

	size_t task_count() {
		std::lock_guard<std::mutex> lock(mtx);
		return tasks.size();
	}
};